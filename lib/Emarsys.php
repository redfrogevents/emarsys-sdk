<?php namespace EmarsysSDK;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\RequestOptions;

class Emarsys
{
    private $api_endpoint = 'https://api.emarsys.net/api/v2/';

    private $token_life;

    protected $http_client;

    protected $secret;

    protected $username;

    /**
     * EmarsysApiClient constructor.
     *
     * @param null        $username
     * @param null|string $secret
     */
    public function __construct($username = null, string $secret = null)
    {
        $this->username = $username;
        $this->secret   = $secret;

        $this->createClient();
    }

    /**
     * Checks connectivity of the client
     *
     * @return bool
     */
    public function checkConnectivity(): bool
    {
        $this->checkTokenLife();
        try {
            $this->http_client->get('settings');
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * Gets all lists
     *
     * @return \Illuminate\Support\Collection
     */
    public function lists()
    {
        $this->checkTokenLife();
        $response = $this->http_client->get('contactlist');

        $lists = collect(\GuzzleHttp\json_decode($response->getBody())->data);

        return $lists;
    }

    /**
     * Returns the created list.
     *
     * @param array $params
     *
     * @return array
     * @throws Exception
     */
    public function createList(array $params)
    {
        $this->checkTokenLife();
        $response = $this->http_client->post('contactlist', [
            RequestOptions::JSON => $params,
        ]);

        $json_response = json_decode((string)$response->getBody());

        return [
            'id'   => $json_response->data->id,
            'name' => $params['name'],
        ];
    }

    public function createContacts($contacts)
    {
        if (count($contacts) >= 1000) {
            throw new Exception("Emarsys's API does not allow the number contacts to be greater than 10000");
        }
        $this->checkTokenLife();

        $this->http_client->post('contact', [
            'json' => [
                'contacts' => $contacts->map(function ($contact) {
                    return [
                        1  => $contact->first_name,
                        2  => $contact->last_name,
                        3  => $contact->email,
                        31 => 1,
                    ];
                })->values(),
            ],
        ]);
    }

    public function campaigns($options = [])
    {
        $response = $this->http_client->get('email/', [
            'query' => $options,
        ]);
        $lists    = collect(\GuzzleHttp\json_decode($response->getBody())->data);

        return $lists;
    }

    public function getLuanchOfCampaign($id)
    {
        $response = $this->http_client->post('email/getlaunchesofemail/', [
            'json' => [
                'emailId' => $id,
            ],
        ]);
        $lists    = collect(\GuzzleHttp\json_decode($response->getBody())->data);

        return $lists;
    }

    /**
     * Batch creates contacts on Emarsys
     * It does it in async to speed up the process if
     * the event had a lot of attendees to sync
     *
     * @param $list_id
     * @param $attendees
     *
     * @throws ConnectException
     */
    public function batchAddMembersToList($list_id, $attendees)
    {
        foreach ($attendees->chunk(999) as $attendee_chunk) {
            $this->createContacts($attendee_chunk);
            $this->attachContactsToList($list_id, $attendee_chunk);
        }
    }

    /**
     * @param string   $list_id
     * @param Attendee $attendee
     */
    public function addContactToList(string $list_id, $attendee)
    {
        $this->checkTokenLife();
        $attendee = collect([$attendee]);
        $this->createContacts($attendee);
        $this->attachContactsToList($list_id, $attendee);
    }

    public function getContact($email)
    {
        $response = $this->http_client->post('contact/getdata', [
            RequestOptions::JSON => [
                'keyId'     => "3",
                "keyValues" => [
                    $email,
                ],
                'fields'    => [
                    "1",
                    "2",
                    "3",
                    "28500",
                    "28499",
                ],
            ],
        ]);

        $json_response = json_decode((string)$response->getBody());

        if (! empty($json_response->data->errors)) {
            throw new Exception('Contact Not Found');
        }
    }

    public function updateContact($email, $event_details, $marketing)
    {
        $response = $this->http_client->put('contact', [
            RequestOptions::JSON => [
                'keyId'    => "3",
                "contacts" => [
                    [
                        "28500" => $event_details,// 1 or 2
                        "28499" => $marketing, // 1 or 2
                        "3"     => $email,
                    ],
                ],
            ],
        ]);
    }

    /**
     * Emarsys requires a X-WSSE header for authentication.
     *
     *  Always use random 'nonce' for increased security.
     * timestamp: the current date/time in UTC format encoded as
     *   an ISO 8601 date string like '2010-12-31T15:30:59+00:00' or '2010-12-31T15:30:59Z'
     * passwordDigest looks sg like 'MDBhOTMwZGE0OTMxMjJlODAyNmE1ZWJhNTdmOTkxOWU4YzNjNWZkMw=='
     *
     * @param string $username
     * @param string $secret
     *
     * @return string
     */
    private function getXWSSEHeader()
    {
        $nonce     = bin2hex(random_bytes(15));
        $timestamp = gmdate("c");
        $digest    = base64_encode(sha1($nonce . $timestamp . $this->secret, false));

        return 'UsernameToken Username="' . $this->username . '", PasswordDigest="' . $digest . '", Nonce="' . $nonce . '", Created="' . $timestamp . '"';
    }

    /**
     * @param $list_id
     * @param $attendee_chunk
     * @param $promises
     */
    private function attachContactsToList($list_id, $attendee_chunk)
    {
        $this->checkTokenLife();
        $this->http_client->post("contactlist/{$list_id}/add", [
            'json' => [
                'external_ids' => $attendee_chunk->pluck('email'),
            ],
        ]);
    }

    private function createClient()
    {
        $this->http_client = new Client([
            'base_uri' => $this->api_endpoint,
            'timeout'  => 60,
            'headers'  => [
                'Content-Type' => 'application/json',
                'X-WSSE'       => $this->getXWSSEHeader(),
            ],
        ]);
        $this->token_life  = strtotime('now');
    }

    /**
     * The token expires after a certain time so we need to refresh it
     *
     */
    private function checkTokenLife()
    {
        if (strtotime('now') - $this->token_life > 120) {
            $this->createClient();
        }
    }
}
